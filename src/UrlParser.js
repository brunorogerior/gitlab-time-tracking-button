export default class UrlParser {
  // parser.protocol; // => "http:"
  // parser.host;     // => "example.com:3000"
  // parser.hostname; // => "example.com"
  // parser.port;     // => "3000"
  // parser.pathname; // => "/pathname/"
  // parser.hash;     // => "#hash"
  // parser.search;   // => "?search=test"
  // parser.origin;   // => "http://example.com:3000"

  constructor(string) {
    const parser = document.createElement('a');
    parser.href = string;
    this.parser = parser;
  }

  getInstanceKey() {
    return `${this.parser.origin}/`;
  }

  getAllData() {
    const result = this.validUrl() ? this.requestMaker() : null;
    if (result instanceof Array) {
      const [, group, subgroup, project, issue] = result;
      return { instance: this.getInstanceKey(), group, subgroup, project, issue };
    }
    return {
      instance: this.getInstanceKey(),
      group: null,
      subgroup: null,
      project: null,
      issue: null,
    };
  }
  requestMaker() {
    const res = this.parser.pathname.match(/\/([a-zA-Z0-9_-]+\/)([a-zA-Z0-9_/-]+)*([a-zA-Z0-9_-]+)\/issues\/(\d+)*/);
    if (res instanceof Array) {
      return res;
    }
    return null;
  }
  getProject() {
    return this.getAllData().project;
  }

  getGroup() {
    return this.getAllData().group;
  }

  getIssueNr() {
    return this.getAllData().issue;
  }
  validUrl() {
    const isValidUrl = this.parser.pathname.match(/\/issues\/(\d+)/);
    if (isValidUrl != null) {
      return true;
    }
    return false;
  }
}
